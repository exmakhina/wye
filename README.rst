###
WYE
###

This repo provides a socket interface to a something with SerialPort. With multiple
connections to it allowed.

The main use of this is to interact with remote SerialPort device from multiple connections
while having same response on each of them.

The connection to device is supported by whether SerialPipe or PySerial.

Usage
#####

Download the wye.py(Note that the exmahkina/konvini repo required as it provides a couple of
required classes and functions).


Use case
########

Here is the way of connecting to device by SerialPipe.

Starting the server and connecting to the device.

.. code:: bash

   python/python3 ./wye.py stdio_command "ssh pi4xm socat stdio open:/dev/USB0,rawer,b115200"

From another terminal/cmd window connect to the server.

.. code:: bash

   socat stdio tcp-connect:localhost:9999
