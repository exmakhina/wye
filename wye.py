#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2016,2022 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT
# Proxy to allow a client (maybe more) to connect by TCP to a devise via SerialPort

"""
This provides a local interface to a remote SerialPort device.
Multiple connections to it are allowed (which may be dangerous).
"""

import socket, select, time, sys, collections, io, os, subprocess
import contextlib
import logging
import shlex

from exmakhina.konvini.listen_and_connect import listener_from_url
from exmakhina.konvini.subprocess import TerminatingPopen
from exmakhina.serialpipe.serialpipe import SerialPipe
from exmakhina.captty.captty import CaptureWriter


logger = logging.getLogger()


def main(argv=None):
	import argparse

	parser = argparse.ArgumentParser(
	 description="TCP listener multiplexing SerialPort device",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	group = parser.add_mutually_exclusive_group()

	group.add_argument("--stdio-command",
	 help="Specifications for getting stdio"
	)
	group.add_argument("--serial-port",
	 help="Specifications to open a serial port"
	)

	parser.add_argument("--listen",
	 default="tcp://localhost:9999",
	 help="Listen address specification",
	)

	parser.add_argument("--capture-rx",
	 help="Path to save data chunks received from the target"
	)

	parser.add_argument("--capture-tx",
	 help="Path to save data chunks sent to the target"
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args(argv)

	try:
		import coloredlogs
		coloredlogs.install(
			level=getattr(logging, args.log_level),
			logger=logger,
		)
	except ImportError:
		logging.basicConfig(
			datefmt="%Y%m%dT%H%M%S",
			level=getattr(logging, args.log_level),
			format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	with contextlib.ExitStack() as stack:
		if args.stdio_command is not None:
			cmd = shlex.split(args.stdio_command)
			proc = TerminatingPopen(cmd,
			 stdin=subprocess.PIPE,
			 stdout=subprocess.PIPE,
			 bufsize=0,
			)
			stack.enter_context(proc)

			serial_pipe = SerialPipe(proc.stdin, proc.stdout)
		elif args.serial_port is not None:
			import serial
			ser = serial.serial_for_url(args.serial_port, timeout=0)
			serial_pipe = SerialPipe(ser, ser)
		else:
			stdin = sys.stdin.buffer
			stdout = sys.stdout.buffer

			serial_pipe = SerialPipe(stdin, stdout)

		stack.enter_context(serial_pipe)

		writer_rx, writer_tx = None, None
		if args.capture_rx:
			writer_rx = CaptureWriter(args.capture_rx)
			stack.enter_context(writer_rx)
		if args.capture_tx:
			writer_tx = CaptureWriter(args.capture_tx)
			stack.enter_context(writer_tx)

		channel = []

		class Server:
			def __init__(self, endpoint, handler, bind_and_activate=None):
				self.socket = endpoint

		server_ = listener_from_url(args.listen, Server, Server)

		server = server_.socket

		timeout = None

		try:
			while True:
				rs = [server, serial_pipe._rx] + channel
				ws = []
				xs = []
				rs, ws, xs = select.select(rs, ws, xs, timeout)
				for r in rs:
					if r is server:
						clientsock, clientaddr = r.accept()
						channel.append(clientsock)
						logger.info("new client: %s", clientaddr)
						break
					elif r is serial_pipe._rx:
						data = serial_pipe.read(4096, timeout=0)
						logger.info("<serial> %s", data)
						if writer_rx is not None:
							writer_rx.write(data)
						if not data:
							logger.info("SerialPort device has disconnected")
							for peer in channel:
								peer.close()
							return

						toremove = []
						for peer in channel:
							try:
								peer.sendall(data)
							except ConnectionResetError:
								toremove.append(peer)

						for peer in toremove:
							channel.remove(peer)

					elif r in channel:
						data = r.recv(4096)
						try:
							host, port = r.getpeername()
							endpoint = f"{host}:{port}"
						except:
							endpoint = "remote"
						logger.info("<%s>: %s", endpoint, data)
						if writer_tx is not None:
							writer_tx.write(data)
						if data:
							serial_pipe.write(data)
						else:
							logger.info("%s has disconnected", endpoint)
							r.close()
							channel.remove(r)
		except KeyboardInterrupt:
			logger.info("Bye")


if __name__ == "__main__":
	ret = main()
	raise SystemExit(ret)
